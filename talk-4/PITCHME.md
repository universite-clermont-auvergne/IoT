# Internet Of Things

## L'internet des objets

@title[Internet Of Things]

---

## XBee

@title[XBee]

---

## XBee / Digi


@title[XBee / Digi]

* XBee est un ensemble de modules de la compagnie Digi
* Basé sur la norme IEE 802.15.4
* Pour les réseaux en étoile et en point à point
* Débit de 250 kbits/sec

Note:

* Plusieurs types de puces:
* * XBee (< 1mW)
* * XBee-PRO ( 100mW )

La société Digi vends aussi tout un tas de produits en liens:

* Adaptateurs
* Passerelles
* Logiciels

---

## XBee / PRO / DigiMesh, ...

@title[XBee/PRO/DigiMesh]

Plusieurs produits dans la famille:

* XBee 802.15.4
* XBee-PRO 802.15.4
* XBee DigiMesh (2.4GHz)
* XBee-PRO DigiMesh (2.4GHz)
* XBee ZB
* XBee-PRO ZB

Et d'autres encore...

Note:

* XBee et XBee-PRO ne prennent en charge que le point-to-point et le star
* DigiMesh permet le Mesh
* XBee ZB permet le Mesh

---

## XBee / Programmation

@title[Programmation]

* Transparent mode
* Packet-based

Note:
Transparent mode:
  La donnée transmise en entrée est envoyée telle-quelle vers one cible (point-to-point) ou en broadcast (en réseau en étoile)
Packet-Base:
  Le développeur contrôle l'ensemble des paramètres de transmission

---

## XBee / Configuration

### Les commandes AT / Hayes

@title[Commandes AT]

* Hayes
* SmartModem 1200 / 2400
* Début de la standardisation des commandes

Note:
Dans les années 80, la société Hayes a fabriqué des modems (Smart) 1200 bauds, très rapidement après le marché de ce type de modems a disparu et les a contraint à créer un modem smart 2400.
De ce fait ils ont recréé le modem 2400 en se basant fortement sur les interfaces du modem 1200. Contre leurs volonté, cela a permit aux sociétés qui utilisaient le premier model de passer sur le nouveau model plus rapide sans changer les commandes de programmation.

---

### Les commandes AT

@title[Configurer le device XBee]

Les commandes AT sont utiles pour :

* Configurer le device XBee

---

### Structure d'une commande AT

---

@title[Structure d'une commande AT]
![Structure d'une commande AT](assets/images/AT_Command_Structure.png)

---

@title[Exemple d'une communication AT]
![Exemple d'une communication AT](assets/images/AT_Command_Sample.png)

---

### Commandes Basiques

---

* AT
* ATCN
* ATWR
* ATRE
* ATFR

Note:

* AT: Vérifie la communication avec le module
* ATCN: Termine explicitement le mode de commande
* ATWR: Sauvegarde la configuration dans la mémoire
* ATRE: Restore defaults
* ATFR: Software reset

---

### Configuration réseau

---

* CH (Channel)
* ID
* DH
* DL

---

## Addressage

### Chaque paquet envoyé contient

* Addresse Source
* Addresse Destination

---

### CH / Channel

* Range : 0xB - 0x1A
* Pour calculer la fréquence: 2405 MHz +(CH - 11) * 5MHz

Note:

Tous les devices doivent avoir le même channel

---

### ID / PAN ID

* Identifiant du réseau auquel le device est connecté
* Range : 0 - 0xFFFF
* Valeur 0xFFFF permet de transmettre sur tous les PAN mais mais de recevoir de tous les PAN

Note:
Tous les devices doivent avoir le même channel

---

### DH / Destination Address High

* Premiers 32bits de l'addresse avec lequel le device communique
* Range : 0 - 0xFFFFFFFF

---

### DL / Destination Address Low

* Derniers 32bits de l'addresse avec lequel le device communique
* Range : 0 - 0xFFFFFFFF

---

### MY

* Identifiant du device sur le réseau
* Range : 0 - 0xFFFF

---

## Addressage

@title[Addressage]

* Chaque device possède 2 addresses
* * Addresse longue : *64 bits* identifiant unique du device
* * Addresse courte : *16 bits* identifiant spécifique au PAN

* Boardcast
* * Tous les devices d'un PAN
* * Sur l'adresse courte : 0xffff

---

## Coordinateur

@title[Coordinateur]

* Chaque PAN possède *un PAN coordinator*
* * Full-Function Device (FFD)
* * Gère toutes les demandes d'entrée et de sortie du réseau
* * Assigne les addresses courtes aux devices

---

## Unicast

@title[Unicast]

* Comportement par défaut
* Communication en peer to peer

Note:
XBee support le mode retries uniquement dans le mode unicast
Permet de relancer les messages jusqu'à 3 fois, jusqu'à ce qu'un ACK soit recu.

---

## Unicast

@title[Unicast]

### Deux types d'addressage

* Addresses courtes
* Addresses longues

---

## Unicast addresses courtes

@title[Unicast addresses courtes]

### Plusieurs conditions doivent être réunies

* MY < 0xFFFE
* DH = 0
* DL < 0xFFFE

---

@title[Unicast addresses courtes]
![xbee unicast short address](assets/images/xbee_unicast_short_address.png)

---

## Unicast addresses longues

@title[Unicast addresses longues]

### Plusieurs conditions doivent être réunies

* Désactiver MY
* * MY = *0xFFFF ou 0xFFFE*
* Addresse de destination de l'expéditeur *(DL + DH)* doit correspondre à l'addresse source *(SL + SH)* du destinataire

---

## Broadcast

@title[Broadcast]

Paramétrer le device comme suit:

* DL = *0xFFFF*
* DH = *0x0000*

Note:
Lors de la configuration, le paramètre doit être mis sans le "0x"

---

## Association

* L'adhésion entre les end devices et le coordinateur du PAN
* Permet de paramétrer automatique les informations réseau :
* * Channel
* * ID de PAN
* * MY
* * ...

+++

## Association - Coordinateur

### Paramètre A2

* Reassign_PANID
* Reassign_Channel

Note:
2 bits.
S'ils sont à 0 alors ce sont les paramétrages CH et PAN ID qui sont pris en compte.

+++

## Reassign_PANID

### Active Scan

Permet de s'auto assigner un PAN ID

* Le coordinateur sélectionne un channel
* Il envoit un une message en broadcast et attends une réponse
* Liste les PANs sur le channel
* Passe au channel suivant

Note:
A la fin du scan, le coordinateur sélectionne un PAN ID qui n'existe pas sur aucun des réseaux

+++

## Reassign_Channel

### Energy Scan

Permet de s'auto assigner un channel

* Sélectionne un channel
* Ecoute le réseaux
* Passe au suivant

Note:
A la fin du scan le coordinateur sélectionne le channel où il y a le moins de perturbations

+++

## Association - End Device

### Paramètre A1 les bits

* Reassign_PANID
* Reassign_Channel
* AutoAssociate

+++

## Association - End Device

### Paramètre A1

* AutoAssociate Bit
* * *1* tente de s'associer à un coordinateur
* * *0* utilise les paramétrages ID CH et MY

Note:
Si le bit est à 1 alors le device effectue un active scan.

+++

### ActiveScan

Pour se configurer le device recherches les coordinateurs

* Sélectionne un channel
* Envoit un message en broadcast sur le channel et les PANs
* Liste les réponses
* Passe au channel suivant

Note:
Une fois l'Active Scan terminé le end device sélectionne le channel et le PAN dont la qualité de transmission est la meilleure.