# IoT - Internet Of Things

## Présentations

### Module 1 - Introduction / Les protocols communs

[![GitPitch](https://gitpitch.com/assets/badge.svg)](https://gitpitch.com/universite-clermont-auvergne/IoT/master?grs=gitlab&t=white&p=talk-1)

Sommaire :

* Qu'est-ce que l'IoT
* Le monde de l'IoT
* Les capteurs
* Le materiel
* Le sans fil
* Topologies de réseaux
* Les protocoles
* Cellular
* WiFi
* Bluetooth - Low Energy
* NFC

### Module 2 - Les protocoles communs

[![GitPitch](https://gitpitch.com/assets/badge.svg)](https://gitpitch.com/universite-clermont-auvergne/IoT/master?grs=gitlab&t=white&p=talk-2)

Sommaire :

* ZigBee
* Z-Wave
* LoRaWAN
* SigFox
* 6LowPAN

### Module 3 - Conclusions

[![GitPitch](https://gitpitch.com/assets/badge.svg)](https://gitpitch.com/universite-clermont-auvergne/IoT/master?grs=gitlab&t=white&p=talk-3)

Sommaire :

* Conclusions

### Module 4 - XBee

[![GitPitch](https://gitpitch.com/assets/badge.svg)](https://gitpitch.com/universite-clermont-auvergne/IoT/master?grs=gitlab&t=white&p=talk-4)

Sommaire :

* Digi
* PRO / DigiMesh, ...
* Programmation
* Configuration
* Les commandes AT / Hayes
* * Structure d'une commande AT

### Module 5 - Traveaux pratiques

[![GitPitch](https://gitpitch.com/assets/badge.svg)](https://gitpitch.com/universite-clermont-auvergne/IoT/master?grs=gitlab&t=white&p=talk-5)

Sommaire :

* Mise en oeuvre d'un prototype - Arduino
* Recucpération de données de capteurs
* Connexion à un réseau ZigBee
* Echange de données entre modules sur un réseau ZigBee
