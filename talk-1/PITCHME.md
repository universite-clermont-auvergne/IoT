# Internet Of Things

## L'internet des objets

@title[Internet Of Things]

---

@title[Auteur]

## Kevin BEAUGRAND

### Expert technique chez CGI

__mail__: kevin.beaugrand@cgi.com

---

## Qu'est-ce que l'IoT

---

## Le monde de l'IoT

+++

@title[Les contextes]
![IoT Word](assets/images/IoT_World_1.png)

Note:
allumer une lampe à distance, régler le chauffage, vérifier la sécurité de son domicile…
les montres, bracelets, t-shirts connectés, les drones
opérations avec le dossier médical affiché sur des lunettes connectées, ou suivies à distance grâce au même équipement dans une optique de formation…
pilotage à distance, conseils en direct…
machines qui gèrent elles-mêmes leurs réparations, qui alertent en cas d’avarie, commandent des matières premières…

+++

@title[Les interactions]
![IoT Word](assets/images/IoT_World_2.png)

Note:
Comment l'IoT façonnent la façon dont nous interagissons avec eux.
Reconnaissance vocale, reconnaissance gestuelle, ...

+++

@title[L'acquisition]
![IoT Word](assets/images/IoT_World_3.png)

Note:
Les capteurs, le matériel, le logiciel, la connectivité, les passerelles

+++

@title[L'analyse]
![IoT Word](assets/images/IoT_World_4.png)

Note:
Cloud Platform, Gestion des ressources, analyse de données, Machine Learning, tableaux de bord et outils
Edge Intelligence => traiter les données au plus proche des sensors et n'envoyer dans la data que celles utiles

+++

@title[L'integration]
![IoT Word](assets/images/IoT_World_5.png)

Note:
Data market places, block chains.

---

@title[En sommes]
![IoT Word](assets/images/IoT_World.png)

---

@title[L'acquisition]
![IoT Word](assets/images/IoT_World_3.png)

---

@title[Les capteurs]

## Les capteurs

### Quelles données puis-je capter sur le monde réel ?

+++

@title[Carte des capteurs]
![Les capteurs](assets/images/sensors.png)

+++

@title[Les fabricants]
![Les capteurs](assets/logos/sensors_manufacturers.png)

---

@title[Le materiel]

## Le materiel
### Comment puis-je réaliser mon besoin ?

+++

## Le materiel

@title[Prototypage v.s industrialisation]
* Le prototypage
* L'industrialisation

+++

## Le materiel
### Prototypage

@title[Prototypage]

* RaspBerry
* Arduino
* Intel UpSquared
* Samsung ARTIK
* Qualcomm DragonBoard
* Libelium
* ...

---

@title[Le logiciel]

## Le logiciel
### Comment prototyper ?

+++

## Le logiciel

### RaspBerry
@title[RaspBerry]

* OS: Linux, Windows
* Language: C, C++, C#, Javascript, Python, Java, ...
* IDE: Atom, VSCode, Visual Studio, Eclipse, ...

+++

@title[Le logiciel]

## Le logiciel

### Arduino
@title[Arduino]

* OS: N/A
* Language: (C++/JavaScript)
* IDE: Arduino IDE

---

## La communication
### Comment récupérer les données recueillies ?

+++

@title[Le sans fil]
## Le sans fil

* Ondes Radios 
* Sécurité
* Fréquence
* Canal
* Les topologies de réseaux

+++

## Le sans fil
### Ondes radio
@title[Ondes radio]

* Transmission par difussion

+++

## Le sans fil
### La sécurité

* Tout le monde reçoit ce qui a été emit par un device
* Pas besoin d'être proche pour pirater un réseaux

-> Nécessite des protocoles de cryptage et d'authentification

Note: 
Exemples de risques : 
- Sondes
  Exploitation de données privés

- SmartHome : 
  Ouverture de portes automatiques sans authentification

...

+++ 

## Le sans fil
### Fréquence
@title[Fréquence]

* Une fréquence de base
* Un signal analogique qui porte les données numériques

+++

## Le sans fil
### Fréquence / la physique
@title[Fréquence / La physique]

* Basse fréquence -> Longue distance
* Haute fréquence -> Courte distance

+++

## Le sans fil
### Canal

@title[Canal]

* Un canal est une fréquence particulière
* Une bande de fréquence divisée en plusieurs canaux permet autant de communications simultanées

+++

## Le sans fil
### Les topologies de réseaux

+++

@title[Star network]

![Star network](assets/images/StarNetwork.svg.png)

Note: 
Star network : Réseau en étoile
* Réseau typique du Wifi

+++

@title[Tree network]

![Tree network](assets/images/TreeTopology.png)

Note: 
Tree network : Réseau en arbre
Ce sont plusieurs réseaux en étoile reliés par un bus
* Ressemble aux réseaux filaires
* Peut être résalisé avec un bus filaire

Exemple: Les réseaux ethernet d'entreprise

+++

@title[Mesh network]

![Mesh network](assets/images/NetworkTopology-Mesh.svg.png)

Note:
Mesh network : Réseau maillé
* Permet de réaliser une tolérance de panne sur des noeud, 
* Permet d'augmenter les distances de communications sans cout d'infrastructure
* Necessite en outre un paramétrage pour connaitres les routes possibles de communication

Exemple: l'internet lui-même

---

## Les protocoles
@title[Les protocoles]

+++

@title[Les protocoles]

## Les protocoles

* Cellular
* Wifi
* BlueTooth
* NFC
* ...

+++ 

## Les protocoles

### IEEE 802

@title[IEEE 802.XX]

* 802.11 - Réseaux locaux sans fil
* 802.15 - Réseaux personnels sans fil (WPAN)
* 802.15.3 - Réseaux personnels sans fil à haut débit (High Rate WPAN)
* 802.15.4 - Réseaux personnels sans fil à bas débit (Low Rate WPAN)
* 802.15.5 - Réseaux sans fil maillés interopérables - Haut débit et Bas débit (Mesh Networking)

Note: 
Institute of Electrical and Electronics Engineers ou IEEE est une associations d'acteurs du monde de l'électronique et de la télécomunication qui ont pour but de promouvoir la conaissance dans le monde de l'électronique.
A ce titre l'institut publie des revues scientfiques et un certain nombre de normes
Le groupe de travail 11 est en charge de l'établissement des spécifications pour l'implémentation de réseaux numériques locaux sans fils

+++

@title[La norme 802.11 dans le modèle OSI]
![La norme 802.11 dans le modèle OSI](assets/images/OSI-80211e.png)

## Les protocoles

---

@title[Cellular]

## Les protocoles

### Cellular

+++

### Cellular

#### Plusieurs générations

@title[Plusieurs générations]

* GSM
* GRPS
* 3G
* 4G

+++

### Cellular

#### Le GSM

@title[Le GSM]

* Global System for Mobile Communications
* Réseaux de type Voix
* Créé en 1982
* GPRS et 3G (UMTS) sont basés sur le GSM

Note: 
Développé par ETSI : European Telecommunications Standards Institute

+++

### Cellular

#### Les services disponibles

@title[Les services disponibles]

* Communication par la voix
* Appels d'urgence
* Short Message Service (SMS)
* Accès au service de données et à l'internet (entre 300 et 9600 kbps)

+++ 

### Cellular

#### Les normes

@title[Les normes]

Deux grands standard du GSM :

* GSM 900 : Concu pour une très grande couverture géographique (Campagnes)
* GSM 1800 : Concu pour une converture géographique à grande densité de mobiles (Villes)

+++

### Cellular

#### GSM 900

@title[GSM 900]

Communications montantes : 890-915 MHz
Communications déscendantes : 935-960 MHz
Cannaux : 200KHz
Nombre de cannaux : 125

+++

### Cellular

#### GSM 1800

@title[GSM 1800]

Communications montantes : 1710-1785 MHz
Communications déscendantes : 1805-1880 MHz
Cannaux : 200 KHz
Nombre de cannaux : 375

Note:
1 canal -> 1 seul emetteur/recepteur

+++

### Cellular

#### En france

@title[En France]

+++

@title[Le réseau GSM en France]
![Le réseau GSM en France](assets/images/GSM_France_Bandwith.PNG)

Note:
GSM-R : Réseau dédié aux communications férroviaires non compatible GSM
En GSM 900 : Chaque opérateur dispose de 10MHz divisé en 2 pour GSM et UMTS (5MHz)
En GSM 1800 : Tous les opérateurs sauf Free disposent de 20MHz (toujours divisé par 2)

GSM 900 :
10MHz / 200 KHz => 50 cannaux 25 pour GSM, 25 pour UMTS
25 communications simultanés dans une cellule

GSM 1800 :
50 communications simultanés dans une celulle

+++

### Cellular

#### AMRT ou TDMA

@title[AMRT]

Accès multiple à répartition dans le temps

* Division des emetteurs dans le temps
* Permet 8 communications simultanées par canal physique

Note:
TDMA : Time division multiple access

+++

### Cellular

#### AMRF ou FDMA

@title[AMRF]

Accès multiple à répartition en fréquence

Note:
FDMA: Frequency Division Multiple Access

+++

### Cellular

#### Et la securité '?'

@title[Et la sécurité ?]

* Chiffrement par protocol A5/1
* * Basé sur une clef de chiffrement synchrone
* Authentification du mobile par IMEI (SIM)
* Pas d'authentification du réseaux -> Attaque par "man in the middle" possible

Note:
Chiffrement craqué depuis 1997
En 2010, Karten Nohl démontrait sa solution pour hacker le réseaux GSM et écouter des conversations en direct
Ecoutes de la NSA dévoilés en 2013
Néanmoins le protocole et inchangé, puisque pour le craquer cela necessite une très grosse puissance de calculs

+++

### Cellular

#### Quid de la 4G

* 4G LTE utilise une bande de fréquence plus large dans le monde
* *  30 bandes de fréquences en 450MHz et 3.8 GHz

Note:
TDD: Time Division Duplexing
FDD: Frequency Division Duplexing
TDD et FDD possibles en LTE
En france et en europe c'est le FDD qui a été choisi

---

## Les protocoles

### WiFi
@title[WiFi]

+++

### WiFi

@title[Introduction]

* Créé en 1997
* Normes 802.11
* Plusieurs versions cohabitent

+++

### WiFi

@title[Différentes versions]
![Différentes versions](assets/images/Wifi_normes.PNG)

+++

### WiFi

#### La sécurité

@title[La securité]

* WEP - Wifi Equivalent Privacy
* WPA - Wifi Protected Access  (WPA2)

Note: 
L'utilisation de la Clef WEP est très risquée puisque très facilement violable avec de simples logiciels
WPA2 (802.11i) offre un mécanisme de sécurité plutôt fiable mais n'est pas pris en charge par tous les devices

Si WPA2 n'est pas possible, différents méchanismes permettent de s'assurer une sécurité suffisante comme le VPN

---

## Les protocoles

### Bluetooth

@title[Bluetooth]

+++

### Bluetooth

@title[Introduction]

* Créé en 1994
* Normes 802.15.1
* A l'origine créé comme alternative aux cables RS232

+++

### Bluetooth

#### BLE : Bluetooth Low Energy

@title[Différentes versions]

* 4.0 + LE : apparu en 2010, permet l'interconnexion d'appereils connectés en tout genre
* 4.1 : apparu en 2013, permet la connexion de multiples appareils sur un point d'accès
* 4.2 : apparu en 2014, permet l'utilisation du protocol IP pour la communication entre les appareils
* 5.0 : apparu en 2016, augmente le débit et le rayon d'action

Note: 
4.0: première version permettant la diffusion de restituion musicale avec une qualité comparable au MP3

+++

#### BLE : Bluetooth Low Energy

@title[Chiffres clefs]

* Bande de fréquence : 
          2.4 GHz-2.485 GHz
          40 canaux de 2 MHz
* Débit :
          avant 4.0 -> 2Mbit/s (3Mbit/s avec EDR)
          en 4.0 -> 25Mbit/s
          en 5.0 -> 50 Mbit/s
* Distance :
          < 100 m en classe 1 (< 10m pour la classe 3)
* Consommation :
          > 0.01W < 0.5W (en fonction de l'usage), ~1W sans LE

Note:
EDR : Enhance Data Rate
4.0: première version permettant la diffusion de restituion musicale avec une qualité comparable au MP3

+++

#### BLE : Bluetooth Low Energy

@title[La sécurité]

3 modes de sécurité possible :
* 1 : pas de sécurité
* 2 : sécurité au niveau service (uniquement les données utilisateur)
* 3 : sécurité sur le lien

Note: 
Le mode de sécurité doit être pris en charge sur les deux devices qui communiquent entre eux.

---

## Les protocoles

### NFC
@title[NFC]

+++

### NFC
@title[Introduction]


* Créé en 1983 sous le nom RFID
* Utilisé pour le partage de données entre 2 devices sur de courtes distances
* Pas de configuration préalable nécessaire

+++

### NFC
@title[Chiffres clefs]


* Fréquence : 13.56 MHz
* Débit : 424 kbit/s
* Consommation : 0 W
* Distance : < 20cm

+++

### NFC
@title[La sécurité]

Note: 
La faible distance rends, la communication difficilement hackable
Par contre, il n'y a pas de cryptage natif des données

Il est possible ce pendant de crypter manuellement avec un clef asymétrique les données des tags afin de s'assurer qu'aucune opération de piratage ne puisse être fait: 
- Man in the middle
- Lecture des données frauduleuses
- Corruption des données