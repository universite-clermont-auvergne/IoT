## Les protocoles

---
## Les protocoles

### ZigBee
@title[ZigBee]

+++

### ZigBee
@title[Introduction]


* Concu en 1998, standardisé en 2003 et révisé en 2006
* Concu pour réaliser des réseaux de communications 
* * entre plusieurs devices, 
* * de faible débit 
* * de courte portée
* * très faible consommation d'énergie
* Norme : IEEE 802.15.4

+++

### ZigBee
@title[Les roles]

En fonction des topologies différents roles sont à mettre en oeuvre :

* Coordinateur
* Routeur
* Noeud

Note: 
Dans tous les cas, il est nécessaire de disposer d'un coordinateur ZigBee
En reséau en étoile le coordinateur est le noeud central
En réseau tree et mesh, il est possible d'ajouter en plus un routeur pour étendre le réseaux

+++

### ZigBee
@title[Les roles/Topologies]

![Les roles/Topologies](assets/images/Network_topology.png)

+++

### ZigBee
@title[Chiffres clefs]

* Fréquence :
* * 784 MHz en Chine
* * 868 MHz en Europe
* * 915 MHz en USA et Australie
* * 2.4 GHz dans le monde entier
* Débit : 20 kbit/s en 868 MHz, 250 kbit/s en 2.4 GHz
* Distance : < 20m

+++

### ZigBee
@title[La sécurité]
#### La securité

* Authentification sur le réseau
* Chiffrement par AES
* Contrôle d'intégité

Note: 
ZigBee permet d'intiliser un échange de clefs de chiffrements (SKKE) avec un chiffrement de ces échanges avec une MasterKey

---

## Les protocoles

@title[Z-Wave]
### Z-Wave

+++

@title[Introduction]
## Z-Wave

* Créé en 2001
* Initialement concu pour les réseaux de domotique
* Faible consommation d'énergie.
* Pas de norme IEEE 802.xx
* Support des réseaux de maillage ad-hoc

Note:
La consitution d'un réseaux maillé de Z-Wave nécéssite une configuration via un controller.
Le réseau peut atteindre jusqu'à 232 appareils
Si besoin, il est possible de relier plusieurs réseaux

Le réseaux adhoc permet de faire communiquer des appareils entre eux mêmes s'ils ne sont pas directement accessibles par un principe de rebonds.

+++

### Z-Wave

@title[Chiffres clefs]

* Fréquence :
* * 868.42 MHz en Europe
* * 902.42 MHz en Amérique du nord
* Débit : 40 kbit/s
* Distance : < 40m
* Des millers de capteurs déjà présents sur le marché

+++

@title[La sécurité]
#### La securité

* Authentification sur le réseau
* Chiffrement par AES
* Contrôle d'intégité

Note: 
La clef de chiffrement des communications est échangé au demarrage du réseaux (réinitialisation du controller)
De ce fait il est possible d'intercepter ces échanges pour pirater un réseau Z-Wave
Cependant ce cas de piratage est relativement compliquer à mettre en oeuvre à cet instant précis.

--- 

## Les protocoles
### LoRaWAN
@title[LoRaWAN]

+++

@title[Introduction]
## LoRaWAN

* Créé en 2015
* LoRaWAN pour Long Range Wild Area Network
* Très faible consommation d'énergie
* Puces très peu cher (< 2$)
* Pas de norme IEEE 802.xx
* Réseaux en étoile d'étoiles

Note: 
LoRa est créé par une startup grenobloise (Cycleo) par Semtec
Le but est de fournire une solution peu couteuse pour le déploiement d'objets connectées et leur connexion
-> Réseaux en étoile (simple à mettre en oeuvre)

+++

### LoRaWAN
@title[Chiffres clefs]

* Fréquence :
* 868 MHz en Europe
* Débit : 40 kbit/s
* Paquets de 51 octets à 222 octets
* Distance : 5 Km en zone urbaine
             15 Km en zone rurale
* 
+++

@title[La sécurité]
#### La securité

* Chiffrement par AES
* Contrôle d'intégité

---

## Les protocoles

### SigFox
@title[SigFox]

+++

@title[Introduction]
## SigFox

* Créé en 2009
* Très faible consommation d'énergie
* Pas de norme IEEE 802.xx
* Sigfox déploit ses propres antennes

Note: 
SigFox est créé par une startup Toulousaine (Cycleo)
Le but est de fournire une solution peu couteuse pour le déploiement d'objets connectées et leur connexion

+++

### SigFox

@title[Couverture]
![Couverture géographique](assets/images/SigFox_Coverage.PNG)

+++

### SigFox

@title[Chiffres clefs]

* Fréquence : 868 MHz en Europe
* Débit : 100 bits/s
* Paquets de 12 octets

+++

@title[La sécurité]
#### La securité

* Authentification
* Chiffrement
* Contrôle d'intégité

---

## Les protocoles

### 6LoWPAN
@title[6LoWPAN]

+++

@title[Introduction]
## 6LoWPAN

* Norme 802.15.4
* Concu pour les réseaux à débit limité (< 250 kbits/s>)

Note:
Acronyme de IPv6 Low power Wireless Personal Area Networks
Créé dans le but de pouvoir utiliser le protocole IP dans un réseau de capteurs en diminuant la taille des entêtes
Protocole de base de l'implémentation offerte par le groupe Thread

+++

### 6LoWPAN

* Header compression
* Fragmentation an reassembly
* Stateless auto-configuration

+++

### 6LoWPAN
#### Header compression

* Réduction des entêtes d'état

Note:
Supprimer dans les entêtes les informations considérés comme "par défaut"
Supprimer certaines informations de routage, qui ne sont pas nécessaires dans le cas des communications par radio

+++

@title[Header-Compression]
![Header-Compression](assets/images/6LowPAN_Compression_Headers.png)

Note:

1. Communication entre 2 devices dans le même réseaux, en utilisant l'adresse du Lien-local seulement 2 bits sont nécessaires dans l'entête
2. Communication destinée à un device qui n'est pas dans le réseau 6LoWPAN dont le préfix du réseau est connu. L'entête peut être réduite à 12 bytes
3. Comme le 2 mais en ne connaissant pas le préfix du device, l'entête est de 20 bytes.

+++

### 6LoWPAN
#### Fragmentation an reassembly

* Les paquets IPv6 sont transmis en sous paquets sur le réseaux IEEE 802.15.4

Note:
MTU IPV6: 1280 bytes
MTU IEE 802.15.4: 127 bytes

+++

@title[Fragmentation_Reassembly]
![Fragmentation_Reassembly](assets/images/6LowPAN_Fragmentation_Reassembly.png)

+++

### 6LoWPAN
#### Stateless auto-configuration

* Autoconfiguration
* Neighbor discovery (NDP)

Note:
IPv6 nécessite un serveur DHCP pour se configurer
IPv6 autorise une découverte des voisins et une autoconfiguration de address IPv6

+++

### 6LoWPAN
#### Neighbor discovery (NDP)

* Neighbor solicitation (NS)
* Neighbor advertisement (NA)
* Router solicitation (RS)
* Router advertisement (RA)

Note:
Pour se configurer un device envoit une address qu'il s'est attribué (link-local unicast) sur le réseaux => NS.
Si un device possède la même address il renverra un NA
Si aucun NA n'est recu alors l'addresse est validée
Pour pouvoir communiquer sur le réseau il lui faut ensuite le préfixe réseau.
Le device envoit ensuite un RS. Le routeur répondra par un message RA avec les informations demandées.

+++

@title[Exemple de réseaux mesh 6LoWPAN]
![Exemple de réseaux mesh 6LoWPAN](assets/images/6LowPAN_Network_Sample.png)

+++

@title[La sécurité]
#### La securité

* Chiffrement (AES)
* Contrôle d'intégité