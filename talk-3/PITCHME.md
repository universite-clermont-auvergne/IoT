## Conclusion
@title[Conclusion]

+++ 

## Conclusion
### L'IoT, un monde vaste
@title[L'IoT, un monde vaste]

Note:
* Les contextes
    allumer une lampe à distance, régler le chauffage, vérifier la sécurité de son domicile…
    les montres, bracelets, t-shirts connectés, les drones
    opérations avec le dossier médical affiché sur des lunettes connectées, ou suivies à distance grâce au même équipement dans une optique de formation…
    pilotage à distance, conseils en direct…
    machines qui gèrent elles-mêmes leurs réparations, qui alertent en cas d’avarie, commandent des matières premières…
* Les interactions
    Comment l'IoT façonnent la façon dont nous interagissons avec eux.
    Reconnaissance vocale, reconnaissance gestuelle, ...
* L'acquisition
    Les capteurs, le matériel, le logiciel, la connectivité, les passerelles
* L'analyse
    Cloud Platform, Gestion des ressources, analyse de données, Machine Learning, tableaux de bord et outils
    Edge Intelligence => traiter les données au plus proche des sensors et n'envoyer dans la data que celles utiles
* L'integration
    Data market places, block chains.
+++ 

## Conclusion
### L'aquisition des données
@title[L'aquisition des données]

Note: 
Les capteurs, le matériel, le logiciel, la connectivité, les passerelles

+++
 
## Conclusion
### Prototyper 
@title[Prototyper]

Note: 
Cela permet de tester rapidement et à bas coût...
* RaspBerry
* Arduino
* Intel UpSquared
* Samsung ARTIK
* Qualcomm DragonBoard
* Libelium

+++
 
## Conclusion
### Transmettre les données captées 
@title[Transmettre les données captées]

Note:

* Wifi
* BlueTooth
* NFC
* Celullar
* ZigBee
* Z-Wave
* LoRaWAN
* SigFox
* 6LoWPAN

+++
 
## Conclusion
### Familles de réseaux
@title[Familles de réseaux]

+++
 
## Conclusion
### Familles de réseaux
@title[Champs Proche]

* Champs Proche

Note:

* NFC,
* Bluetooth

+++
 
## Conclusion
### Familles de réseaux
@title[Réseau personel, Domotique]

* Champs Proche
* Réseau personel, Domotique

Note:

* Wifi
* ZigBee
* Z-Wave

+++

## Conclusion
### Familles de réseaux
@title[Réseau étendu]

* Champs Proche
* Réseau personel, Domotique
* Réseau étendu

Note:
Appelé Low-Power Wide-Area Network

* Celullar
* SigFox
* LoRaWAN

+++ 

## Conclusion
### La bande ISM
@title[La bande ISM]

* Industriel, Scientifique et Medicale
* 868 MHz
* Utilisation limitée à 1% du temps

Note:
Pour SigFox : Limité à 140 Messages / Jours

+++

## Conclusion
### Sécuriser les échanges !
@title[Sécuriser les échanges !]

Note:
Certains protocol proposent de la sécurité, d'autre demande à l'implémenter nous même...
La sécurité comporte souvent :

* L'authentification des devices
* Le chiffrement des données
* Le contrôle de l'intégritée des données

+++

## Conclusion
### A chaque protocole ses caractéristiques techniques
@title[A chaque protocole ses caractéristiques techniques]

Note:
Distance, Débit, Cas d'usage, ...

+++

@title[Carte des networks]
![Carte des networks](assets/images/Iot_Network_map.png)

+++

@title[Auteur]

## Kevin BEAUGRAND
### Expert technique chez CGI

__mail__: kevin.beaugrand@cgi.com