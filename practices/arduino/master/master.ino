#include <xBEE.h>

xBEE xbee = xBEE("255C", "777D7EE5DB70B3880A062ED14F5568E6");

void setup() {  
  Serial.begin(9600);
  
  xbee.AutoConfigure();
}

void loop() {
  delay(3000);
  //xbee.Send("test library");
  
  if(!xbee.Available()) { 
    return; 
  }
  
  String input = xbee.Read();

  xbee.Send(input);
}
